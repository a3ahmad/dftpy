__author__ = "Pavanello Research Group"
__contact__ = "m.pavanello@rutgers.edu"
__license__ = "MIT"
__version__ = "0.0.2"
__date__ = "2020-10-16"

from .config import *
from .mpi import mp
