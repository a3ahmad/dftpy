=======
Contact
=======

DFTpy Developers
================

Feel free to contact the developers:
 - `Xuecheng Shao <https://sites.rutgers.edu/prg/people/xuecheng-shao/>`_
 - `Kaili Jiang <https://sites.rutgers.edu/prg/people/kaili-jiang/>`_
 - `Michele Pavanello <https://sasn.rutgers.edu/about-us/faculty-staff/michele-pavanello>`_
 - `Alessandro Genova <mailto: ales.genova@gmail.com>`_

On-line
=======

To find out more about the `Pavanello Research Group <http://sites.rutgers.edu/prg>`_. Or simply send us an email.

GitLab
======

Feel free to create new issues, merge requests or fork your own `DFTpy` on our gitlab page: https://gitlab.com/pavanello-research-group/dftpy

Make sure to let us know about your developments!

References
==========

DFTpy
-----
 - Shao, X., Jiang, K., Mi, W., Genova, A., & Pavanello, M. (2021). DFTpy: An efficient and object‐oriented platform for orbital‐free DFT simulations. Wiley Interdisciplinary Reviews: Computational Molecular Science, 11(1), e1482. (https://doi.org/10.1002/wcms.1482)
