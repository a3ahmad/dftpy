===============
Stable releases
===============


Version 1.0
===========

Download:
 - `GitLab link <https://gitlab.com/pavanello-research-group/dftpy/-/tags/dftpy-1.0>`_

This version supports:
 - OFDFT electron density optimization
 - Latest nonlocal KEDFs
 - Ab initio dynamics
 - Hydrodynamic DFT (TD-OFDFT)
 - I/O for plotting and to several file formats
 - Several examples (also on Jupyter notebooks)
