.. dftpy documentation master file

DFTpy: Density Functional Theory with Python
============================================

DFTpy_ is an orbital-free Density Functional Theory code based on a plane-wave expansion of the electron density developed by PRG_ at `Rutgers University-Newark <http://sasn.rutgers.edu>`_.

.. _Python: https://www.python.org
.. _DFTpy: http://dftpy.rutgers.edu
.. _PRG: https://sites.rutgers.edu/prg

.. toctree::
   :maxdepth: 2

   source/contact
   source/install
   source/tutorials/tutorials
   source/releases



